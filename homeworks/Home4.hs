main = do
    print (show(myMaxIndex [1,5,2,3,4]))
    print (show(maxCount [1,5,3,10,3,10,5]))
    print (show(countBetween [-1,3,100,3]))
    print (show(countBetween [100,3,-1,3]))
    print (show(countBetween [-1,100]))
    print (show(countBetween [1]))
    print (show(pi5 10))
-- | Нахождение максимум и его индекс 
-- >>> myMaxIndex [1,5,2,3,4]
-- (1,5)
myMaxIndex :: [Integer] -> (Int,Integer)
myMaxIndex [] = (-1, 0)
myMaxIndex (x:xs) = help (0, x) 1 xs
    where help a i [] = a
          help a i (x:xs) = if x > (snd a)
                          then help (i, x) (i + 1) xs
                          else help a (i + 1) xs
-- | Количество элементов, равных максимальному
-- >>> maxCount [1,5,3,10,3,10,5]
-- 2
maxCount :: [Integer] -> Int
maxCount [] = 0
maxCount (x:xs) = help (1, x) xs
    where help a [] = (fst a)
          help a (x:xs) = if x > (snd a)
                          then help (1, x) xs
                          else if x == (snd a)
                                then help ((fst a) + 1, x) xs
                                else help a xs

-- | Количество элементов между минимальным и максимальным
-- >>> countBetween [-1,3,100,3]
-- 2
--
-- >>> countBetween [100,3,-1,3]
-- -2
--
-- >>> countBetween [-1,100]
-- 1
--
-- >>> countBetween [1]
-- 0
countBetween :: [Integer] -> Int
countBetween [] = 0
countBetween (x:xs) = help ((0, x), (0, x)) 1 xs
    where help a i [] = fromIntegral(fst(fst a) - fst(snd a))
          help a i (x:xs) = if x > (snd (fst a))
                             then help ((i, x), (snd a)) (i + 1) xs
                             else if x < (snd (snd a))
                                   then help ((fst a), (i, x)) (i + 1) xs