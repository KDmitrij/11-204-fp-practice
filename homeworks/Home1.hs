main = do
    print (show(pi1 30000))
    print (show(pi2 30000))
    print (show(pi3 30000))
    print (show(pi4 30000))
    print (show(pi5 30000))
    print (show(e 1 300))

{- 
 -
 -                4
 - pi = -------------------
 -                1^2
 -       1 + --------------
 -                   3^2
 -            2 + ---------
 -                     5^2
 -                 2 + ----
 -                      ...
 -}
pi1 n | n > 0 = 4 / (1 + (help n 1))
    where help 1 a = a * a
          help n a = (a * a) / (2 + (help (n-1) (a + 2)))
{-
 -                  1^2
 - pi = 3 + -------------------
 -                   3^2
 -           6 + -------------
 -                     5^2
 -                6 + ------
 -                     ...
 -}
pi2 n | n > 0 = 3 + (1 / (help n 3))
    where help 1 a = 6
          help n a = (6 + (a * a) / help (n - 1) (a + 2))

{-
 -                 4
 - pi = ------------------------
 -                   1^2
 -       1 + ------------------
 -                    2^2
 -            3 + -----------
 -                     3^2
 -                 5 + ---
 -                     ...
 -}
pi3 n | n > 0 = 4 / (1 + (help n 1))
    where help 1 a = a ^ 2
          help n a = (a ^ 2) / (1 + 2 * a + (help (n - 1) (a + 1)))
 
{-       4     4     4     4
 - pi = --- - --- + --- - --- + ...
 -       1     3     5     7
 -}
pi4 n | n > 0 = 4 + minus n 1
    where minus 1 a = 0
          minus n a = - 4 / (a * 2 + 1) + plus (n - 1) (a + 1)
          plus 1 a = 0
          plus n a = 4 / (a * 2 + 1) + minus (n - 1) (a + 1)

{-             4         4         4         4
 - pi = 3 + ------- - ------- + ------- - -------- + ...
 -           2*3*4     4*5*6     6*7*8     8*9*10
 -}
pi5 n | n > 0 = 3 + plus n 2
    where plus 1 a = 0
          plus n a = 4 / (product[a..a+2]) + minus (n-1) (a + 2)
          minus 1 a = 0
          minus n a = - 4 / (product[a..a+2]) + plus (n-1) (a + 2)

{-
       x^1     x^2
e^x = ----- + ----- + ... + 1 
        1!      2!
-}
e x n | n > 0 = help x n 1 1 1
    where help x 0 a b c = 1
          help x n a b c = (x * c) / b + help x (n - 1) (a + 1) (b * (a + 1)) (c * x)