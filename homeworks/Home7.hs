
-- Функция декодирования двоичной записи числа
-- (аналогично декодированию азбуки Морзе, но
-- учтите бесконечность дерева -- нужна ленивость)
decodeBinary :: String -> Integer
decodeBinary str = help 0 str where
	help num [] = num
	help num ('0':xs) = help (2*num) xs
	help num ('1':xs) = help (2*num+1) xs	

-- Функция декодирования записи числа в системе
-- Фибоначчи: разряды -- числа Фибоначчи, нет
-- двух единиц подряд:
--    0f = 0
--    1f = 1
--   10f = 2
--  100f = 3
--  101f = 4
-- 1000f = 5
-- 1001f = 6
-- 1010f = 7
--   .....
-- (аналогично декодированию азбуки Морзе, но
-- учтите бесконечность дерева -- нужна ленивость)
decodeFibo :: String -> Integer
decodeFibo str = help 0 1 (reverse str) where
	help prev curr ('1':xs) = prev + curr + help curr (prev + curr) xs
	help prev curr ('0':xs) = help curr (prev + curr) xs          
	help _ _ [] = 0
              
