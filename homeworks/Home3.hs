main = do
    print (show(eval(Div (Plus (Mult (Val 1) (Mod (Val 5) (Val 3))) (Val 3)) (Val 5))))
data Expr = Val Int
          | Plus Expr Expr
          | Minus Expr Expr
          | Mult Expr Expr
          | Div Expr Expr
          | Mod Expr Expr
            deriving (Eq,Show)
eval :: Expr -> Int
eval (Val x) = x
eval (Plus x y) = eval x + eval y
eval (Minus x y) = eval x - eval y
eval (Mult x y) = eval x * eval y
eval (Div x y) = (eval x) `quot` (eval y)
eval (Mod x y) = (eval x) `mod` (eval y)
