main = do
    print (show(deleteInt 1 [1,2,1,3,4,1]))
    print (show(findIndicesInt 1 [1,2,1,3,4,1]))
    print (show(odd(tribo 100)))
    let color1 = Mix 255 255 0
    let color2 = Blue
    print (mixColor color2 color1)
    print (getBlue (mixColor color1 color2))
    print (mixColor1 color2 color1)
    print (getBlue (mixColor1 color1 color2))
    print (mixColor2 color2 color1)
    print (getBlue (mixColor2 color1 color2))
    print (mixColor3 color2 color1)
    print (getBlue (mixColor3 color1 color2))


-- | deletes first arg from second arg
-- >>> deleteInt 1 [1,2,1,3,4]
-- [2,3,4]
--
-- >>> deleteInt 1 [2,3]
-- [2,3]
--
-- >>> deleteInt 1 []
-- []
deleteInt :: Int -> [Int] -> [Int]
deleteInt n old = help n old
    where help n [] = []
          help n (x:xs) = if x==n
                                then help n xs
                                else x :help n xs    

-- | returns list of indices of first arg in second arg
-- >>> findIndices 1 [1,2,1,3,4]
-- [0,2]
--
-- >>> findIndices 1 [2,3]
-- []
findIndicesInt :: Int -> [Int] -> [Int]
findIndicesInt n old = help 0 n old 
    where help i n [] = []
          help i n (x:xs) = if x/=n
                                then help  (i + 1) n xs
                                else i : help (i + 1) n xs

-- | tribo_n = tribo_{n-1} + tribbo_{n-2} + tribo_{n-3}
--   tribo_0 = 1
--   tribo_1 = 1
--   tribo_2 = 1
--
-- >>> tribo 0
-- 1
--
-- >>> tribo 1
-- 1
--
-- >>> tribo 2
-- 1
--
-- >>> tribo 3
-- 3
--
-- >>> odd (tribo 100)
-- True
tribo n = help 1 1 1 (n - 3)
        where help a b c 1 = a + b + c
              help a b c n | n > 0 = help b c (a + b + c) (n - 1)
              help a b c n | n <= 0 = 1

-- Тип Цвет, который может быть Белым, Черным, Красным, Зелёным, Синим, либо Смесь из трёх чисел (0-255)
-- операции сложение :: Цвет -> Цвет -> Цвет
-- (просто складываются интенсивности, если получилось >255, то ставится 255)
-- ПолучитьКрасныйКанал, ПолучитьЗелёныйКанал, ПолучитьСинийКанал :: Цвет -> Int
data Color = White 
           | Black
           | Red
           | Green
           | Blue
           | Mix Int Int Int deriving (Show)

mixColor :: Color->Color->Color
mixColor White _ = White
mixColor _ White = White
mixColor Black a = a
mixColor a Black = a
mixColor Red Red = Red
mixColor Red Green = Mix 255 255 0
mixColor Red Blue = Mix 255 0 255
mixColor Red (Mix _ b c) = Mix 255 b c
mixColor (Mix _ b c) Red = Mix 255 b c
mixColor Green Green = Green
mixColor Green Red = Mix 255 255 0
mixColor Green Blue = Mix 0 255 255
mixColor Green (Mix a _ c) = Mix a 255 c
mixColor (Mix a _ c) Green = Mix a 255 c
mixColor Blue Blue = Blue
mixColor Blue Red = Mix 255 0 255
mixColor Blue Green = Mix 0 255 255
mixColor Blue (Mix a b _) = Mix a b 255
mixColor (Mix a b _) Blue = Mix a b 255
mixColor (Mix x y z) (Mix a b c) = Mix (if ((a + x) > 255) then 255 else (a + x)) (if ((b + y) > 255) then 255 else (b + y)) (if((c + z) > 255) then 255 else (c + z))
--Есть ли более адекватный способ это всё записать, кроме как убрать лишние классы и оставить только Mix?

--После смешивания возвращает только конструктор Mix, менее точный, зато 4 строчки против 20
mixColor1 :: Color->Color->Color
mixColor1 (Mix x y z) (Mix a b c) = Mix (if ((a + x) > 255) then 255 else (a + x)) (if ((b + y) > 255) then 255 else (b + y)) (if((c + z) > 255) then 255 else (c + z))
mixColor1 (Mix x y z) a =  Mix (if ((getRed(a) + x) > 255) then 255 else (getRed(a) + x)) (if ((getGreen(a) + y) > 255) then 255 else (getGreen(a) + y)) (if((getBlue(a) + z) > 255) then 255 else (getBlue(a) + z))
mixColor1 a (Mix x y z) =  Mix (if ((getRed(a) + x) > 255) then 255 else (getRed(a) + x)) (if ((getGreen(a) + y) > 255) then 255 else (getGreen(a) + y)) (if((getBlue(a) + z) > 255) then 255 else (getBlue(a) + z))
mixColor1 a b =  Mix (if ((getRed(a) + getRed(b)) > 255) then 255 else (getRed(a) + getRed(b))) (if ((getGreen(a) + getGreen(b)) > 255) then 255 else (getGreen(a) + getGreen(b))) (if((getBlue(a) + getBlue(b)) > 255) then 255 else (getBlue(a) + getBlue(b)))


--золотая середина, 11 строчек кода
mixColor2 :: Color->Color->Color
mixColor2 White _ = White
mixColor2 _ White = White
mixColor2 Black a = a
mixColor2 a Black = a
mixColor2 Red Red = Red
mixColor2 Green Green = Green
mixColor2 Blue Blue = Blue
mixColor2 a b = mixColor1 a b

--использование фабрики? 10 строк и более точный результат
mixColor3 :: Color->Color->Color
mixColor3 (Mix x y z) (Mix a b c) = whatColor(Mix (if ((a + x) > 255) then 255 else (a + x)) (if ((b + y) > 255) then 255 else (b + y)) (if((c + z) > 255) then 255 else (c + z)))
mixColor3 (Mix x y z) a =  whatColor(Mix (if ((getRed(a) + x) > 255) then 255 else (getRed(a) + x)) (if ((getGreen(a) + y) > 255) then 255 else (getGreen(a) + y)) (if((getBlue(a) + z) > 255) then 255 else (getBlue(a) + z)))
mixColor3 a (Mix x y z) =  whatColor(Mix (if ((getRed(a) + x) > 255) then 255 else (getRed(a) + x)) (if ((getGreen(a) + y) > 255) then 255 else (getGreen(a) + y)) (if((getBlue(a) + z) > 255) then 255 else (getBlue(a) + z)))
mixColor3 a b =  whatColor(Mix (if ((getRed(a) + getRed(b)) > 255) then 255 else (getRed(a) + getRed(b))) (if ((getGreen(a) + getGreen(b)) > 255) then 255 else (getGreen(a) + getGreen(b))) (if((getBlue(a) + getBlue(b)) > 255) then 255 else (getBlue(a) + getBlue(b))))

--можно ли запихнуть это как внутреннюю функцию функии MixColor?
whatColor :: Color->Color
whatColor (Mix 255 0 0) = Red
whatColor (Mix 0 255 0) = Green
whatColor (Mix 0 0 255) = Blue
whatColor (Mix 0 0 0) = Black
whatColor (Mix 255 255 255) = White
whatColor a = a



getRed :: Color->Int
getRed White = 255
getRed Red = 255
getRed (Mix a _ _) = a
getRed _ = 0

getGreen :: Color->Int
getGreen White = 255
getGreen Green = 255
getGreen (Mix a b c) = b
getGreen _ = 0

getBlue :: Color->Int
getBlue White = 255
getBlue Blue = 255
getBlue (Mix a b c) = c
getBlue _ = 0