--Тесты
main = do
    let x = Variable "x"
    let z = Application (Abstract "a" (Variable "a")) $ Variable "v"
    let w = Application (Application (Abstract "x" (Abstract "y" (Application (Variable "y") (Variable "x")))) (Variable "x")) (Variable "z")
    print(toTermD(z))
    let z = Variable "z"
        s = Application (Variable "s")
        lamSZ = Abstract "s" . Abstract "z"
        zero = lamSZ z
        one = lamSZ $ s z
        two = lamSZ $ s (s z)
        plus m n = Application (Application (Abstract "m" $ Abstract "n" $ lamSZ $ Application (Application (Variable "m") (Variable "s")) (Application (Application (Variable "n") (Variable "s")) (Variable "z"))) m) n
        tru = Abstract "x" $ Abstract "y" $ Variable "x"
        fls = Abstract "x" $ Abstract "y" $ Variable "y"
        iff b t f = Application (Application b t) f
    print $ evalMulti $ plus zero one -- Application (Application (plus zero one) (Variable "s'")) (Variable "z'")
    print $ evalMulti $ iff tru fls tru

    print(evalMulti(z))
--Термы
data Term = Variable String
          | Abstract String Term
          | Application Term Term
          | VariableD String
          | AbstractD Term
          | ApplicationD Term Term deriving (Show)

--Используем индексы де Брауна
toTermD (Variable x) = VariableD x --Свободные переменные не заменяем индексами
toTermD (Application x y) = ApplicationD (toTermD x) (toTermD y) 
toTermD (Abstract x y) = AbstractD (help (x:[]) y 1)
    where help [] (Variable x) _ = VariableD x
          help (x:xs) (Variable y) index = if y == x 
                                         then VariableD (show index)
                                         else help xs (Variable y) (index + 1)
          help a (Abstract y z) _ = AbstractD (help (y:a) z 1)
          help a (Application y z) _ = ApplicationD (help a y 1) (help a z 1)

--Один шаг
eval (Variable x) = VariableD x
eval (Abstract x y) = toTermD (Abstract x y)
eval (Application x y) = eval(toTermD (Application x y))
eval (VariableD x) = VariableD x
eval (AbstractD x) = AbstractD x
eval (ApplicationD (Variable x) y) = ApplicationD (Variable x) y
eval (ApplicationD x y) = insert x y 0
    where insert (VariableD x) y i = if x == (show i)
                                   then y
                                   else VariableD x
          insert (AbstractD x) y 0 = insert x y 1
          insert (AbstractD x) y index = AbstractD (insert x y (index+1))
          insert (ApplicationD x y) z index = ApplicationD (insert x z index) (insert y z index)

--Шагаем до победного
evalMulti (Variable x) = VariableD x
evalMulti (Abstract x y) = toTermD (Abstract x y)
evalMulti (Application x y) = evalMulti(eval(toTermD (Application x y)))
evalMulti (VariableD x) = VariableD x
evalMulti (AbstractD x) = AbstractD x
evalMulti (ApplicationD (VariableD x) y) = ApplicationD (Variable x) y
evalMulti (ApplicationD x y) = evalMulti(eval(ApplicationD x y))